# Vacation

## Requesting time off

* Make sure to book your vacation:
    * For 1 day off, 3 days in advance
    * For 2 days to 1 week off, by Thursday evening of the week preceding the sprint in which time off starts
    * For more than a week off, two weeks in advance.
* Check the [team calendar](https://www.google.com/calendar/embed?src=auscaqbrvc0uatk6e7126614kk%40group.calendar.google.com)
  first to see if anyone else has booked those dates off. To ensure maximal availability and
  responsiveness within the cell, if the number of persons corresponding to 20% of the cell size
  (rounded up) are already away on some days, you can't book the same days. There is however
  a tolerated 1 day/week per person that can overlap. If in doubt, ask Xavier.
* If you decide to travel and still work, it's useful to let everyone know, but it's not necessary
  to go through the vacations process. As long as you have a good internet connection and maintain
  work hours and communication / reactivity levels, you can work from anywhere.

### Additional steps for core developers

* Check if you need to make any trades on the
  [rotations schedule](https://docs.google.com/spreadsheets/d/1ix68BsU2hJ2ikexXWeBcqFysfoF4l7IKRghIDQgyfPs/edit#gid=447257869)
  and if so, get tentative agreement from someone to trade with you. If the spreadsheet doesn't
  yet cover the week(s) you'll be away, there is a place near the bottom where you can add a note,
  so that we can account for your vacation when the schedule gets updated.
* Make sure that the epic reviewer for any epic you own is going to be present and is fully
  informed so they can take over the epic owner duties while you are away.

### Special vacation

Besides the "normal" vacation, we also offer the following two variants:

* *Reduced time off*: A temporary reduction of the number of hours you work each week.
* *Scoped time off*: A temporary change of the scope of work you will work on, to reduce availability to/from 
  specific projects, clients or roles. This is usually used to allow to focus on one or several specific
  projects for a time, or to take time off projects without being fully off.

The same process and limit as for normal vacations applies. Simply also mention the amount of time that 
you will have available, or your exact scope of work, for that period of time while you announce your vacations.

### Sick days 

* When you are sick and unable to work, let the rest of the team know asap using the 
  [Announcements forum category](https://forum.opencraft.com/c/announcements/7), and let people
  know where you would need help/cover as much as possible. In particular, mention which tasks
  are at risk of spillover or have a looming deadline.
* Then, rest! Don't half work while sick - it will take you longer to recover, and your work will 
  likely not be very good. 
* Note that it is also possible, and encouraged, to take
  [sick days for mental health](https://doist.com/blog/mental-health-and-remote-work/), when you need it.
  Just like other sick days, use them to rest and disconnect - don't check emails or tickets!

## Announcing your vacation

* Once you have confirmed everything, post on the [vacation thread on the forum](https://forum.opencraft.com/t/vacation-announcement-thread/145)
  to announce your vacation, including all the details: 
    * Dates of the vacation (inclusive - ie May 1-3 would be 3 days off)
    * Who is covering for you, for each active epic, rotation and role you have
      responsibilities for
* Add an event on the OpenCraft calendar, for the period of the vacation, with:
    * Your name
    * Whether you are off, or on reduced time (and if so, how much)
    * A link to the forum post
* Note that the announcement with the proper details is what confirms the vacation time. You
  are responsible for ensuring that your vacation time respects the rules - if it doesn't, then
  the time isn't confirmed as off, and the announcement will need to be updated, on both the forum
  and the calendar. In case of doubt, don't hesitate to ask Xavier for a review.

# Vacation checklist

Checklists to go through before going on vacation for each type of role, as well
as checklists for backups of a specific role during the vacation.

## Developer

1. If you own any epics, transfer any knowledge needed to the epic reviewer, and make
   sure they are still prepared to plan/review all the needed stories and manage the
   epic while you are away.
1. Similarly, for all of your [roles](https://handbook.opencraft.com/en/latest/roles/)
   and rotations, identify a backup (and for rotations like firefighting, see if the 
   other firefighter will be off the same days as you), and inform them of the days they
   will need to cover for you. 
1. The sprint before you are away, check if there are any newcomers reviews coming up
   during the time you will be away. If so, coordinate with the cell recruitment manager
   to schedule and send your review early.
1. At least a few days before, make sure that every task or review that's assigned to
   you will either be completed before you leave or assigned to someone else while
   you are away. Ensure that each task won't spill over, and if there is any risk of
   a spillover, proactively address the situation now by asking for help. If you
   will be away for an entire sprint, make sure that there will be absolutely no tasks
   or reviews in that sprint that are assigned to you, including tasks waiting for
   upstream review/blockers.
1. The day before you leave, make sure to set a vacation responder
   indicating dates you will be unavailable and an alternative person and / or
   email to contact in case of urgency (notify the person(s) covering for you).
   [Instructions for setting a vacation responder in Gmail](https://support.google.com/mail/answer/25922?co=GENIE.Platform%3DDesktop&amp;hl=en)

## CEO

### A) Before going away

1. Warn:
    1. Team: send an email with the dates and give/remind emergency contact info
        * Including CTO as backup
        * If Product Specialist will be away at the same time, also mention the need to monitor contact@
    1. Clients & prospects, as necessary
    1. Accountants & lawyers, as necessary
1. Meetings: 
    1. Schedule a preparation meeting on the last day with the CTO
    1. Mark other meetings as "no" and block time in calendar to prevent calendly from adding new meetings
    1. Tell backup what to attend, and cancel others (eg. client meetings)
    1. Set autoresponder, mentioning backup contact
    1. Update the [pager schedule]
1. Invoices (team, clients) - check in advance for vacation in the team, to see if some people need to send an invoice earlier

[pager schedule]: https://opencraft.app.eu.opsgenie.com/teams/dashboard/1114af08-343b-40e0-a9a3-37d56cfb37f8/main

### B) Main backup (CTO)

1. Accepting work from new leads (cf C) Backup - Product Specialist)
1. Announce upgrade to latest version of Open edX if one is released during this period.
   To prepare in advance, and post on the same or next day it is released, as a reply on the
   edx-code@ ML thread by edX announcing it, and on the OpenCraft.Hosting newsletter.

### C) Backup - Product Specialist

1. Prospects management: replying, meeting them, providing quotes & terms, scheduling meetings
   for when CEO is back if needed (via Calendly)
1. Handle email arriving in contact@opencraft.com

## CTO

### A) Before going away

1. List the reponsibilities and tasks of the main backup (see B)
1. Identify backups & create/clone tickets assigned to them, linked to the checklists on this page:
    1. Main backup (CEO)
    1. Sprint reviewer
    1. Ops reviewer - add to ops@opencraft.com and to the pager
1. Warn:
    1. Team: send an email with the dates
    1. Each current client in advance, as well as the edX Open Source team; tell them to contact the
       backup for emergencies
    1. Email: Set auto-responder, mentioning backup contact
    1. Pager: Update the [pager schedule]
1. Sprint/epic knowledge transfer:
    1. Review any tasks in "External Review/Blocker" or "Long External Review/Blocked" that are
       assigned to the CTO and transfer knowledge to someone else on the team
    1. Update each active epic description & write final update in the tickets comments before
       leaving, for knowledge transfer: status, schedule, plans, concerns, commitments, etc.
1. Meetings:
    1. Schedule review & pre-planning meeting on the last day (involve sprint reviewer)
    1. Review all upcoming meetings during time off and ask if backup can attend or the meeting
       organizer knows nobody from OpenCraft can make it.
    1. Schedule review & pre-planning meeting on the last day (involve sprint reviewer)
    1. Ensure time is marked as "unavailable" in Google Calendar and Calendly
    1. Review OpenCraft meeting lead schedule and trade meeting times with others as needed
1. Prepare an invoice for the current month, if the end of the month (invoice time) will happen
   during the vacation.

### B) Main backup

1. Sprint planning:
    1. On Thursday and/or Friday of the second week of a sprint, work with epic owners to review
       the priority of all tickets in the backlog for the upcoming sprint.
    1. Subscribe to GitHub notifications for each PR attached to issues in "Long External
       Review/Blocked", so that you know if upstream starts reviewing them.
1. Sprint supervising:
    1. Check on individual task ETA during sprint to ensure completion
    1. Keep clients informed of progress & answering their questions
    1. Help to unblock anyone who is unable to work on their ticket
1. Attend meetings with potential or current clients to provide technical insight, planning, and estimates
1. Handover completed projects to the clients

## Product Specialist

### A) Before going away

1. Warn CEO and CTO about upcoming prospect work/quotes
1. Backup for contact@ email : CEO, CTO when CEO is also on vacation
1. Do important client follow-ups, let them know about vacation and who will answer them in the meantime
1. Create list of trial instances to archive and schedule dev task
1. Check Pro & Teacher users billing status
1. Last day:
    1. Set autoresponder, mentioning backup contact (CEO or CTO)

### B) Main backup

1. Answer contact@ emails
    1. Offer meeting
    1. Attach "Getting Started With OC" pdf
    1. Schedule discovery task 
    1. Prepare & share quote 
1. Review all emails in spam folder
1. Answer emails and support request from Pro & Teacher users
1. Spawn new Appservers when Pro & Teacher users makes theming changes
