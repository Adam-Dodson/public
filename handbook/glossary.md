# Glossary

There's plenty of jargon that goes around which may overwhelm a newcomer. See [edX's glossary](http://edx.readthedocs.io/projects/edx-partner-course-staff/en/latest/glossary.html) for any terms that may not be listed here.

If you feel there's a piece of lingo that is neither covered by [edX's glossary](http://edx.readthedocs.io/projects/edx-partner-course-staff/en/latest/glossary.html) nor by this document, [let us know](https://gitlab.com/opencraft/documentation/public/-/issues), or open a merge request to add it in.

## Apros

A custom LMS front-end, used by one of our clients (Yonkers). It replaces the standard Open edX LMS front-end.

## Code drift

This refers to changes in forks that aren't present in upstream. Code drift must be maintained (ported, tested, sometimes rewritten) across new release versions, and so adds to our maintenance costs. OpenCraft aims to minimize code drift by upstreaming as much as possible and consolidating the rest into common branches that are shared among several client sites.

## Discovery

The process of "discovering" the set of tasks from client requirements. This is usually the first step when an [epic](#epic) is created. A completed discovery involves scoping the tasks, making [time estimations](how_to_do_estimates.md), and assessing the level of effort needed for the work.

## Epic

A big chunk of work that has one common objective. It could be a feature, customer request or business requirement. These would be difficult to estimate or to complete in a single iteration. Epics contain smaller tasks meant for iterative completion over one or more sprints.

## Firefighter

A firefighter is a sprint's facilitator. The [firefighter's responsibilities](roles.md#firefighter) include handling emergencies, attempting to unblock people, watching over potential spillovers, and more.

## IDA

Short for "Independently Deployable Application". These are separate applications which may integrate with Open edX via
APIs or by sharing authentication.

## Ocim

Short for "OpenCraft Instance Manager". It's our in-house, open source deployment service for Open edX instances,
which:

* Manages configuration and automates deployments for client production sites.
* Provides continuous integration by watching our PRs against the `edx/edx-platform` repository and automatically spinning up a sandbox for the PR's version of the platform. See [how to spin up sandboxes](https://gitlab.com/opencraft/documentation/private/blob/master/howtos/sandboxes.md).

## OVH

The <a href="https://www.openstack.org/">OpenStack</a>-based cloud computing service we use to host OCIM VMs and more. See [OVH's homepage](https://www.ovh.co.uk/).

## OSPR

Short for "Open Source Pull Request". This is the edX process for reviewing pull requests from the open source community.

## Points

Also known as "story points", these represent the approximate "level of effort" and time a task would require. See [Task
Workflows](task_workflows.md#general-tasks) for a description of how we use points. The cells collectively vote on story
points for the tasks in the upcoming sprint, see [Process for sprints](process.md) for details.

## Timebox

Maximum amount of time allocated to a task -- going over the timebox is not permitted. If it looks like there is a risk of going over the timebox, ping Braden and/or the epic owner to discuss.

We sometimes also use timeboxes to manage our sprint commitments. For instance, if a task needs to be started during a
given sprint, but the assignee does not have time to complete it, we may timebox the task to a maximum amount of hours
for the current sprint, and complete the remaining work during the next sprint. Because this affects sprint commitments,
this type of task timeboxing must be decided before the sprint begins, and agreed to by the epic owner.
