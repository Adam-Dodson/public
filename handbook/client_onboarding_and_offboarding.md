# Onboarding and offboarding clients

This section describes the processes we use when we welcome new clients and say goodbye to existing ones. 

## Once a client has accepted our estimates

If the proposal resulting from the discovery is accepted, we move its **epic** to "Accepted" and set a time budget and a timeline based on estimates from the discovery. Discoveries and epics are related through their estimates: tasks in an epic use the estimates and budget of the features the client selected from the quote, which was based on the discovery document. You can validate this with Gabriel.

## New client onboarding

Here's the workflow we use when a new client accepts our quote:

[![Client Onboarding workflow](https://docs.google.com/drawings/d/e/2PACX-1vQg2vl0awtHZro8gDdkaxtGKZipmlcET7RcitXUsfPsLfi2fCxMr1zhzMtGYJh7sihPPnY1RYFdOVHp/pub?w=1349&h=614)](https://docs.google.com/drawings/d/15RceotA5oqKNr0gQCR924B9OnbY5xy6E7b2xezaIkyc/edit?usp=sharing)


## Offboarding process for departing clients

We use the following steps to complete the offboarding of an existing client:

[![Client Offboarding workflow](https://docs.google.com/drawings/d/e/2PACX-1vSPf0UTwORLh-9uSIvVmfE6K9qs65zLUl3JleVYmQ4cX3REzs_bnmtoX_2gYdxfSMTQOE4gw1v7-3Kl/pub?w=1222&h=518)](https://docs.google.com/drawings/d/1n2MGglI0eK9pN-9k6UPyI5cj110M5k0ojQpZmhSbWqQ/edit?usp=sharing)

During the offboarding process, the following [technical document](https://gitlab.com/opencraft/documentation/private/blob/master/ops/client_data.md) provides guidelines on how to handle client data.


